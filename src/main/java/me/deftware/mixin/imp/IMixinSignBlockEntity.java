package me.deftware.mixin.imp;

import net.minecraft.text.Text;

public interface IMixinSignBlockEntity {

    Text[] getTextRows();
}
